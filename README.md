#  WTO（寻找原图）开发文档 V2.0
- WTO全称 `WhereIsTheOriginalImage`,一款专为设计师打造的本地原图查找工具。
- 本地调用的轻量级图像处理模块，仅需下载src目录下的相关文件即可实现即时使用。

## 🖥️ window可运行 程序（.exe）
- 即使是没有编程能力的朋友也能轻松体验这个功能模块【[点此下载](https://gitee.com/chjoo/PixelNumber/raw/04ec09577708ed31db79992397bef3b8fd1da12b/dist/main.exe "windows打包")】。
> 注意：若链接失效，请直接下载项目dist文件夹内的exe执行文件。

## ✨ 前置条件
- 本地需要安装OpenCV、Numpy和psutil及Pillow为核心的Python第三方库。
> 若本地项目已安装，则可无需重复执行该步骤，径直进行后续开发工作。
```Shell
#下载与安装 OpenCV
pip install opencv-python

#下载与安装 Numpy
pip install numpy

#下载与安装 Pillow
pip install Pillow

#下载与安装 psutil
pip install psutil
```
## ⛰️ 导入模块
- 请获取本开源项目的src目录下所提供的核心模块，以便于本地便捷地启用相应的功能组件。
```Python
import src.ImgPro as ImgPro
```
## 🛠️ API

### 1. ImgPro（图像处理）
#### `image_digitization` 结构

> 通过分析图像的像素将图像转化为数字
- @Parameters

<table>
    <tr>
        <td>参数</td> 
        <td>说明</td> 
        <td>数据类型</td>
        <td>默认值</td>
        <td>可否为空</td>
        <td>可选参数</td>
   </tr>
    <tr>
  		<td>Proportion</td> 
        <td>数据精准度</td> 
        <td>int</td> 
  		<td>N</td> 
        <td>N</td> 
        <td>1-50</td>
    </tr>
    <tr>
        <td>BeforeConversion</td> 
        <td>需要转换的文件地址</td> 
        <td>str</td>
  		<td>N</td> 
        <td>N</td> 
        <td></td>
    </tr>
    <tr>
        <td>AfterTheChange</td> 
        <td>转换后图片保存的地址</td> 
        <td>boolean / str</td>
  		<td>False</td> 
        <td>Y</td> 
        <td></td>
    </tr>
</table> 

- @Return
<table>
    <tr> 
        <td>参数</td>
        <td>数据类型</td>
        <td>说明</td>
        <td>默认值</td>
   </tr>
    <tr>
        <td>DataStream</td> 
  		<td>int</td> 
        <td>图像的数据值</td> 
        <td>N</td>
    </tr>
    <tr>
        <td>False</td> 
  		<td>boolean</td> 
        <td>执行失败</td> 
        <td>Y</td>
    </tr>
</table> 

#### `image_pixel_change` 结构

> 转换图像像素并导出图像
- @Parameters

<table>
    <tr>
        <td>参数</td> 
        <td>说明</td> 
        <td>数据类型</td>
        <td>默认值</td>
        <td>可否为空</td>
        <td>可选参数</td>
   </tr>
    <tr>
        <td>File_in</td> 
        <td>需要转换的文件地址</td> 
        <td>str</td>
  		<td>N</td> 
        <td>N</td> 
        <td></td>
    </tr>
    <tr>
        <td>Width</td> 
        <td>需要改变像素的宽</td> 
        <td>int</td>
  		<td>N</td> 
        <td>N</td> 
        <td></td>
    </tr>
    <tr>
        <td>Height</td> 
        <td>需要改变像素的高</td> 
        <td>int</td>
  		<td>N</td> 
        <td>N</td> 
        <td></td>
    </tr>
    <tr>
        <td>File_out</td> 
        <td>转换后图片保存的地址</td> 
        <td>str</td>
  		<td>N</td> 
        <td>N</td> 
        <td></td>
    </tr>
</table> 

- @Return
<table>
    <tr> 
        <td>参数</td>
        <td>数据类型</td>
        <td>说明</td>
        <td>默认值</td>
   </tr>
    <tr>
        <td>File_out</td> 
  		<td>int</td> 
        <td>返回保存后的地址</td> 
        <td>Y</td>
    </tr>
    <tr>
        <td>False</td> 
  		<td>boolean</td> 
        <td>执行失败</td> 
        <td>Y</td>
    </tr>
</table> 

#### `image_similarity` 结构

> 图像相似度差值
- @Parameters

<table>
    <tr>
        <td>参数</td> 
        <td>说明</td> 
        <td>数据类型</td>
        <td>默认值</td>
        <td>可否为空</td>
        <td>可选参数</td>
   </tr>
    <tr>
        <td>ImageNumber1</td> 
        <td>图片一的数字化数据</td> 
        <td>int</td>
  		<td>N</td> 
        <td>N</td> 
        <td></td>
    </tr>
    <tr>
        <td>ImageNumber2</td> 
        <td>图片二的数字化数据</td> 
        <td>int</td>
  		<td>N</td> 
        <td>N</td> 
        <td></td>
    </tr>
</table> 

- @Return
<table>
    <tr> 
        <td>参数</td>
        <td>数据类型</td>
        <td>说明</td>
        <td>默认值</td>
   </tr>
    <tr>
        <td>SimilarityNumber</td> 
  		<td>str</td> 
        <td>图像差值百分比</td> 
        <td>N</td>
    </tr>
    <tr>
        <td>False</td> 
  		<td>boolean</td> 
        <td>执行失败</td> 
        <td>Y</td>
    </tr>
</table> 

#### `is_it_the_same_image` 结构

> 判断两张图片是否相同实例化
- @Parameters

<table>
    <tr>
        <td>参数</td> 
        <td>说明</td> 
        <td>数据类型</td>
        <td>默认值</td>
        <td>可否为空</td>
        <td>可选参数</td>
   </tr>
    <tr>
        <td>File_in1</td> 
        <td>图片一地址</td> 
        <td>str</td>
  		<td>N</td> 
        <td>N</td> 
        <td></td>
    </tr>
    <tr>
        <td>File_in1</td> 
        <td>图片二地址</td> 
        <td>str</td>
  		<td>N</td> 
        <td>N</td> 
        <td></td>
    </tr>
    <tr>
        <td>Proportion</td> 
        <td>识别准确率</td> 
        <td>int</td>
  		<td>50</td> 
        <td>Y</td> 
        <td>1-50</td>
    </tr>

</table> 

- @Return
<table>
    <tr> 
        <td>参数</td>
        <td>数据类型</td>
        <td>说明</td>
        <td>默认值</td>
   </tr>
    <tr>
        <td>True</td> 
  		<td>boolean</td> 
        <td>执行成功</td> 
        <td>Y</td>
    </tr>
    <tr>
        <td>False</td> 
  		<td>boolean</td> 
        <td>执行失败</td> 
        <td>Y</td>
    </tr>
</table>

### 2. OsLook（系统查看模块 - 适配Window电脑）
#### `find_png_files_via_cmd` 结构

> 查看磁盘的JPG和PNG格式的图片
- @Parameters

<table>
    <tr>
        <td>参数</td> 
        <td>说明</td> 
        <td>数据类型</td>
        <td>默认值</td>
        <td>可否为空</td>
        <td>可选参数</td>
   </tr>
    <tr>
  		<td>drive</td> 
        <td>磁盘的名字</td> 
        <td>str</td> 
  		<td>C:</td> 
        <td>Y</td> 
        <td></td>
    </tr>
</table> 

- @Return
<table>
    <tr> 
        <td>参数</td>
        <td>数据类型</td>
        <td>说明</td>
        <td>默认值</td>
   </tr>
    <tr>
        <td>ImageAddress</td> 
  		<td>Array</td> 
        <td>图片地址</td> 
        <td>N</td>
    </tr>
    <tr>
        <td>False</td> 
  		<td>boolean</td> 
        <td>执行失败</td> 
        <td>Y</td>
    </tr>
</table> 

#### `disk_lookup` 结构

> 查看本地全部磁盘信息
- @Parameters
<table>
    <tr>
        <td>参数</td> 
        <td>说明</td> 
        <td>数据类型</td>
        <td>默认值</td>
        <td>可否为空</td>
        <td>可选参数</td>
   </tr>
    <tr>
  		<td>NULL</td> 
        <td>无需传参</td> 
        <td>NULL</td> 
  		<td>N</td> 
        <td>Y</td> 
        <td></td>
    </tr>
</table>
- @Return
<table>
    <tr> 
        <td>参数</td>
        <td>数据类型</td>
        <td>说明</td>
        <td>默认值</td>
   </tr>
    <tr>
        <td>disks</td> 
  		<td>Object</td> 
        <td>磁盘信息</td> 
        <td>N</td>
    </tr>
    <tr>
        <td>False</td> 
  		<td>boolean</td> 
        <td>执行失败</td> 
        <td>Y</td>
    </tr>
</table> 

## ⏫ 建议反馈
> 若有任何宝贵建议，欢迎随时与我联系，您的建议将是我优化代码的重要动力来源。
>> 📮：www_chjoo@163.com



