import json
import subprocess
import time
import os

import psutil as psutil


def find_png_files_via_cmd(drive='C:\\'):
    """
       @Description: 查找磁盘图片
       @Author  : CHJOO
       @Time    : 2024/3/10
       @Parameters :
           - drive (str): 磁盘名字
       @Return :
           - ImageAddress (Array): 图片地址
    """
    try:
        command = f'dir /s /b {drive}*.png;{drive}*.jpg'
        output = subprocess.check_output(command, shell=True, text=True)
    except Exception as e:
        return False
    return output.splitlines()


def disk_lookup():
    """
       @Description: 查找电脑全部磁盘
       @Author  : CHJOO
       @Time    : 2024/3/10
       @Parameters :
           - NULL
       @Return :
           - disks - 磁盘信息
    """
    disks = psutil.disk_partitions(all=False)
    return disks


def terminal_debugging_interface(disks):
    """
       @Description: 终端调试ShellUI界面
       @Author  : CHJOO
       @Time    : 2024/3/10
       @Parameters :
           - NULL
       @Return :
           - NULL
    """
    UIData = "WTO-2.0.1\n" + "1.查看本地磁盘\n" + ""
    return UIData


def read_json(FileName):
    """
       @Description: 打开并加载json文件
       @Author  : CHJOO
       @Time    : 2024/3/10
       @Parameters :
           - FileName(str):文件名
       @Return :
           - loaded_data(dictionary):字典数据
    """
    try:
        with open(FileName, 'r') as f:
            loaded_data = json.load(f)
    except Exception as e:
        return False
    return loaded_data


def write_json(FileName, data):
    """
       @Description: 写入json文件
       @Author  : CHJOO
       @Time    : 2024/3/10
       @Parameters :
           - FileName(str):文件名
           - dara(dictionary):写入数据
       @Return :
           - boolean
    """
    try:
        with open(FileName, 'w') as f:
            json.dump(data, f)
    except Exception as e:
        return False
    return True

def file_exists(FileName):
    """
       @Description: 是否存在文件
       @Author  : CHJOO
       @Time    : 2024/3/10
       @Parameters :
           - FileName(str):文件名
       @Return :
           - boolean
    """
    current_dir = os.getcwd()
    file_path = os.path.join(current_dir, FileName)
    return os.path.isfile(file_path)

if __name__ == '__main__':
    print(find_png_files_via_cmd(1))
