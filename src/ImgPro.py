import cv2
import numpy as np
from PIL import Image


def image_digitization(Proportion, BeforeConversion):
    """
       @Description: 图像数字化
       @Author  : CHJOO
       @Time    : 2024/3/5
       @Parameters :
           - Proportion (int): 准确率 (50-1越低准确率越高)
           - BeforeConversion (str): 需要数字化的文件名
       @Return :
           - DataStream (int): 图像数据
    """
    try:
        img = cv2.imread(BeforeConversion)
    except Exception as e:
        print("未找到该文件，请输入正确的格式")
        return False

    if img is not None:
        if len(img.shape) == 2:
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        # 直接计算所有像素颜色分量之和
        total_sum = img.sum(axis=(0, 1)).sum()
        return total_sum
    else:
        return False


def image_pixel_change(File_in, Width, Height, File_out):
    """
       @Description: 图像像素改变
       @Author  : CHJOO
       @Time    : 2024/3/5
       @Parameters :
           - File_in (str): 需要改变像素的文件名
           - Width (int): 需要改变像素的宽
           - Height (int): 需要改变像素的高
           - File_out (str): 需要改变后像素的文件名
       @Return :
           - File_out (int): 需要改变后像素的文件名
    """
    try:
        image = Image.open(File_in)
        resized_image = image.resize((Width, Height), Image.ANTIALIAS)
        resized_image.save(File_out)
    except Exception as e:
        return False
    return File_out


def image_similarity(ImageNumber1, ImageNumber2):
    """
       @Description: 图像相似度比例
       @Author  : CHJOO
       @Time    : 2024/3/5
       @Parameters :
           - ImageNumber1 (int): 图片1的数据
           - ImageNumber2 (int): 图片2的数据
       @Return :
           - SimilarityNumber (str): 图像相似度
    """
    if ImageNumber1 == 0 or ImageNumber2 == 0:
        return False
    Similarity = (min(ImageNumber1, ImageNumber2) / max(ImageNumber1, ImageNumber2)) * 100
    SimilarityNumber = str(round(Similarity, 4)) + "%"
    return SimilarityNumber


def is_it_the_same_image(File_in1, File_in2, Proportion=50):
    """
       @Description: 图像是否相同
       @Author  : CHJOO
       @Time    : 2024/3/5
       @Parameters :
           - File_in1 (str): 图片名
           - File_in2 (str): 图片名
           - Proportion (int): 准确率 (50-1越低准确率越高) (默认50)
       @Return :
           - isSame (boolean): 是否相同
           - SimilarityNumber (str): 图像相似度
    """
    if image_pixel_change(File_in1, 1001, 1001, "ContrastA.png") is False or image_pixel_change(File_in2, 1001, 1001,
                                                                                                "ContrastB.png") is False:
        print("请正确输入图片名")
        return False
    ContrastA = image_digitization(Proportion, "ContrastA.png")
    ContrastB = image_digitization(Proportion, "ContrastB.png")
    SimilarityNumber = image_similarity(ContrastA, ContrastB)
    if SimilarityNumber is False:
        print("请不要拿纯黑色图片进行测试")
        return False
    print("这两张图片的相似度：", SimilarityNumber)
    if float(SimilarityNumber[:-1]) >= 99.5:
        print("测试结果：相同")
    else:
        print("测试结果：不同")
    return True
