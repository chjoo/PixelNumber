import src.OsLook as OL
import src.ImgPro as IP
import time

if __name__ == '__main__':
    FileName = "ImgUrlData.json"
    if OL.file_exists(FileName):
        start = time.perf_counter()
        loaded_data = OL.read_json(FileName)
        print(len(loaded_data['E:\\'])+len(loaded_data['C:\\'])+len(loaded_data['D:\\']))
        Urls = loaded_data['E:\\'][:1000]
        for url in Urls:
            IP.image_pixel_change(url, 101, 101, "ContrastA.png")
            data = IP.image_digitization(1, "ContrastA.png")
            # print(data)
        end = time.perf_counter()
        runTime = end - start
        print("运行时间：", runTime, "秒")
    else:
        start = time.perf_counter()
        disks = OL.disk_lookup()
        ImgUrls = {}
        for disk in disks:
            device = disk.device
            ImgUrl = OL.find_png_files_via_cmd(device)
            ImgUrls[device] = ImgUrl
            print(device, "已存储完成")
        end = time.perf_counter()
        runTime = end - start
        print("运行时间：", runTime, "秒")
        OL.write_json(FileName, ImgUrls)
